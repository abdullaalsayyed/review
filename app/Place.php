<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'api_token',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'distance', 'avgLoves', 'avgReviews', 'isLoved', 'totalReviews'
    ];

    /**
     * Initial LatLng for calculating distance
     *
     * @var array
     */
    public $userLocation = [30, 31];

    /**
     * Count reviews for this place.
     *
     * @return int
     */
    public function getTotalReviewsAttribute()
    {
        return $this->reviews()->count() ?? 0;
    }

    /**
     * Check if logged in user loved this place.
     *
     * @return bool
     */
    public function getIsLovedAttribute()
    {
        if(Auth::check())
            return !!$this->loves()->where(['user_id' => Auth::user()->id])->first();
        return false;
    }

    /**
     * Calculate distance between place and logged in user.
     *
     * @return float
     */
    public function getDistanceAttribute()
    {
        return round($this->haversineGreatCircleDistance($this->userLocation[0], $this->userLocation[1], $this->lat, $this->lng)/1000, 0);
    }

    /**
     * Calculate average reviews for this place.
     *
     * @return float|int
     */
    public function getAvgReviewsAttribute()
    {
        return round($this->reviews()->avg('amount'), 1) ?? 0;
    }

    /**
     * Calculate loves count for this place.
     *
     * @return int
     */
    public function getAvgLovesAttribute()
    {
        return $this->loves()->count() ?? 0;
    }

    /**
     * Get place's reviews
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reviews()
    {
        return $this->hasMany(PlaceReview::class);
    }

    /**
     * Get place's loves
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function loves()
    {
        return $this->hasMany(PlaceLove::class, 'place_id');
    }

    /**
     * Calculates the great-circle distance between two points, with the Haversine formula.
     * @param float $latitudeFrom Latitude of start point in [deg decimal]
     * @param float $longitudeFrom Longitude of start point in [deg decimal]
     * @param float $latitudeTo Latitude of target point in [deg decimal]
     * @param float $longitudeTo Longitude of target point in [deg decimal]
     * @param int $earthRadius Mean earth radius in [m]
     * @return float Distance between points in [m] (same as earthRadius)
     */
    function haversineGreatCircleDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
    {
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
                cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        return $angle * $earthRadius;
    }
}
