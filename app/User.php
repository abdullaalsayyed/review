<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'api_token',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'avgReviews', 'totalReviews',
    ];

    /**
     * Calculate user's average reviews.
     *
     * @return int|mixed
     */
    public function getAvgReviewsAttribute()
    {
        return $this->reviews()->avg('amount') ?? 0;
    }

    /**
     * Count all user's reviews.
     *
     * @return int
     */
    public function getTotalReviewsAttribute()
    {
        return $this->reviews()->count() ?? 0;
    }

    /**
     * Get user's reviews.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reviews()
    {
        return $this->hasMany(PlaceReview::class, 'user_id');
    }

    /**
     * Get user's loves.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function loves()
    {
        return $this->hasMany(PlaceLove::class, 'user_id');
    }
}
