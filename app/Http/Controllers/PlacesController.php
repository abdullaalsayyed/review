<?php

namespace App\Http\Controllers;

use App\Place;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PlacesController extends Controller
{
    /**
     * Get all places based on filters and sorters.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(Request $request)
    {
        if (!$request->get('lat') || !$request->get('lng') || !$request->get('category_id') || !$request->get('sortBy') || !$request->get('min_review') || !$request->get('max_review'))
            return response()->json([
                'status'    => 200,
                'message'   => 'category_id, sortBy, lat, lng, min_review and max_review are required'
            ]);

        if ($request->get('category_id') === 'all')
            $places = Place::all();
        else
            $places = Place::where([ 'category_id' => $request->get('category_id') ])->get();

        $sortedPlaces = [];
        foreach ($places as $place)
            if ($place->avgReviews >= $request->get('min_review') && $place->avgReviews <= $request->get('max_review'))
                $sortedPlaces[] = $place;
        $places = json_decode($places->toJson());

        if ($request->get('sortBy') === 'most_loved')
            usort($places, function ($a, $b) {
                return $a->avgLoves < $b->avgLoves;
            });
        else if ($request->get('sortBy') === 'near')
            usort($places, function ($a, $b) {
                return $a->distance > $b->distance;
            });
        else if ($request->get('sortBy') === 'far')
            usort($places, function ($a, $b) {
                return $a->distance < $b->distance;
            });
        else if ($request->get('sortBy') === 'most_review')
            usort($places, function ($a, $b) {
                return $a->avgReviews < $b->avgReviews;
            });

        return response()->json([
            'status'    => 200,
            'data'      => $places,
        ]);
    }

    /**
     * Toggle love for a certain place.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function love(Request $request)
    {
        if (!$request->get('place_id'))
            return response()->json([
                'status'    => 400,
                'message'   => 'place_id is required',
            ]);

        if (!Auth::check())
            return response()->json([
                'status'    => 401,
                'message'   => 'You must be logged in to use this service',
            ]);

        if (! ($place = Place::find($request->get('place_id'))))
            return response()->json([
                'status'    => 404,
                'message'   => 'Place not found',
            ]);

        $user = Auth::user();

        if ($love = $place->loves()->where(['user_id' => $user->id])->first())
            $love->delete();
        else
            $place->loves()->create(['user_id' => $user->id]);

        $place = $place->fresh();

        return response()->json([
            'status'    => 200,
            'data'      => [
                'isLoved' => $place->isLoved,
            ]
        ]);
    }

    /**
     * Show a single place information
     *
     * @param Int $place_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function single(Int $place_id)
    {
        if (!$place_id || !($place = Place::find($place_id)))
            return response()->json([
                'status'    => 400,
                'message'   => 'Place not found',
            ]);

        if(!Auth::check())
            return response()->json([
                'status'    => 401,
                'message'   => 'You must be logged in',
            ]);

        $review = $place->reviews()->where(['user_id' => Auth::user()->id])->first();

//        $place->increment('views');

        return response()->json([
            'status'    => 200,
            'data'      => [
                'place'     => $place,
                'review'    => $review ? $review->amount : 0,
            ]
        ]);
    }

    /**
     * Review or update review for a single place.
     *
     * @param Int $place_id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function review(Int $place_id, Request $request)
    {
        if(!$place_id || !$request->get('review'))
            return response()->json([
                'status'    => 400,
                'message'   => 'place_id and review are required',
            ]);

        if(!Auth::check())
            return response()->json([
                'status'    => 401,
                'message'   => 'You must be logged in',
            ]);

        if(! ($place = Place::find($place_id)))
            return response()->json([
                'status'    => 404,
                'message'   => 'Place not found',
            ]);

        if($review = $place->reviews()->where(['user_id' => Auth::user()->id])->first()) $review->update(['amount' => $request->get('review')]);
        else $review = $place->reviews()->create(['user_id' => Auth::user()->id, 'amount' => $request->get('review')]);

        return response()->json([
            'status'    => 200,
            'data'      => [
                'place'     => $place->fresh(),
                'review'    => $review->fresh()->amount,
            ],
        ]);
    }
}
