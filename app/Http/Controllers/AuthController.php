<?php

namespace App\Http\Controllers;

use App\PlaceReview;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    /**
     * Login using email and password
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        if(!$request->has('email') || !$request->has('password')) {
            return response()->json([
                'status'    => 400,
                'message'   => 'email and password are required',
            ]);
        }

        if (! ($user = User::where(['email' => $request->get('email')])->first())) {
            return response()->json([
                'status'    => 401,
                'message'   => 'email not found',
            ]);
        }

        if (!Hash::check($request->get('password'), $user->password)) {
            return response()->json([
                'status'    => 401,
                'message'   => 'wrong password',
            ]);
        }

        $api_token = str_random(40);

        $user->update(['api_token' => $api_token]);

        return response()->json([
            'status'    => 200,
            'data'      => [
                'api_token' => $api_token,
            ],
            'message'   => 'logged in successfully'
        ]);
    }

    /**
     * Register user using email and password
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        if(!$request->has('email') || !$request->has('password') || !$request->has('name')) {
            return response()->json([
                'status'     => 400,
                'message'    => 'name, email and password are required',
            ]);
        }

        if (User::where(['email' => $request->get('email')])->first()) {
            return response()->json([
                'status'    => 400,
                'message'   => 'email already used',
            ]);
        }

        $password = Hash::make($request->get('password'));
        $api_token = str_random(40);

        User::create(['name' => $request->get('name'), 'email' => $request->get('email'), 'password' => $password, 'api_token' => $api_token]);

        return response()->json([
            'status'    => 200,
            'data'      => [
                'api_token' => $api_token,
            ]
        ]);
    }

    /**
     * Get logged in user profile information.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function profile()
    {
        if(!Auth::check())
            return response()->json([
                'status'    => 401,
                'message'   => 'You must be logged in',
            ]);

        return response()->json([
            'status'    => 200,
            'data'      => [
                'profile'   => Auth::user(),
                'reviews'   => PlaceReview::where(['user_id' => Auth::user()->id])->orderBy('created_at', 'desc')->get(),
            ]
        ]);
    }

    /**
     * Update logged in user profile inoformation
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        if(!Auth::check())
            return response()->json([
                'status'    => 401,
                'message'   => 'You must be logged in',
            ]);

        if($request->get('password'))
            Auth::user()->update(['password' => Hash::make($request->get('password'))]);

        if ($request->get('name'))
            Auth::user()->update(['name' => $request->get('name')]);

        return response()->json([
            'status'    => 200,
            'data'      => [
                'profile'   => Auth::user(),
                'reviews'   => PlaceReview::where(['user_id' => Auth::user()->id])->orderBy('created_at', 'desc')->get(),
            ]
        ]);
    }
}
