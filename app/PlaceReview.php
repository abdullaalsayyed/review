<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlaceReview extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'place_id', 'user_id', 'amount',
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['place', 'user'];

    /**
     * Get parent place
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function place()
    {
        return $this->belongsTo(Place::class);
    }

    /**
     * Get reviewer
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
