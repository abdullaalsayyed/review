<?php

use Illuminate\Database\Seeder;

class PlacesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        factory(App\Place::class, 10)->create();
//        factory(App\PlaceReview::class, 30)->create();
        factory(App\PlaceLove::class, 30)->create();
    }
}
