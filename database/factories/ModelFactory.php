<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'api_token' => '',
        'password'  => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
    ];
});

$factory->define(App\Place::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->title,
        'image' => $faker->imageUrl(),
        'category_id' => $faker->numberBetween(1, 3),
        'lat'   => $faker->randomFloat(null, 25, 35),
        'lng'   => $faker->randomFloat(null, 25, 35)
    ];
});


$factory->define(App\PlaceReview::class, function (Faker\Generator $faker) {
    return [
        'user_id' => $faker->numberBetween(1, 10),
        'place_id' => $faker->numberBetween(1, 10),
        'amount'    => $faker->randomFloat(1, 1, 5),
    ];
});


$factory->define(App\PlaceLove::class, function (Faker\Generator $faker) {
    return [
        'user_id' => $faker->numberBetween(1, 10),
        'place_id' => $faker->numberBetween(1, 10),
    ];
});


$factory->define(App\Category::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->title
    ];
});
