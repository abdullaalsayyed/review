<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'auth'], function () use ($router) {

    $router->post('login', [
        'uses' => 'AuthController@login',
    ]);

    $router->post('register', [
        'uses' => 'AuthController@register',
    ]);

    $router->group(['prefix' => 'profile'], function () use ($router) {

        $router->post('/', [
            'uses'  => 'AuthController@profile',
        ]);

        $router->post('update', [
            'uses'  => 'AuthController@update',
        ]);
    });
});

$router->group(['prefix' => 'places'], function () use ($router) {

    $router->post('/', [
        'uses' => 'PlacesController@get',
    ]);

    $router->post('love', [
        'uses' => 'PlacesController@love',
    ]);

    $router->group(['prefix' => '{place_id}'], function () use ($router) {

        $router->post('/', [
            'uses' => 'PlacesController@single',
        ]);

        $router->post('review', [
            'uses' => 'PlacesController@review',
        ]);
    });
});
